#This file is part health_patient_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.model import ModelView
from trytond.wizard import Wizard, StateTransition, StateAction, StateView, Button
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.pyson import PYSONEncoder
from trytond.i18n import gettext

from ..exceptions import NoRecordSelected, NoDU

class OpenPatientDU(Wizard):
    'Create Appointment Evaluation'
    __name__ = 'wizard.gnuhealth.patient.du'

    start_state = 'patient_du'
    patient_du = StateAction('health_patient_fiuner.act_patient_du')

    def do_patient_du(self, action):
        patient = Transaction().context.get('active_id')
        try:
            patient_id = \
                Pool().get('gnuhealth.patient').browse([patient])[0]
        except:
            raise NoRecordSelected(gettext('health_patient_fiuner.msg_no_record_selected'))
        try:
            du_id = patient_id.name.du.id
        except:
            raise NoDU(gettext('health_patient_fiuner.msg_no_du'))

        data = {'res_id': [du_id]}
        action['views'].reverse()
        return action, data
