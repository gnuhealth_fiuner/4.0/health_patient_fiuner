#This file is part health_patient_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.pool import Pool
from . import health
from .wizard import wizard_patient_du

def register():
    Pool.register(
        health.Party,
        health.PatientData,
        health.PatientSESAssessment,
        health.Appointment,
        module='health_patient_fiuner', type_='model')
    Pool.register(
        wizard_patient_du.OpenPatientDU,
        module='health_patient_fiuner', type_='wizard')
