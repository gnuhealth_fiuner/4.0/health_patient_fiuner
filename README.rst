.. image:: https://www.gnuhealth.org/downloads/artwork/logos/isologo-gnu-health.png

GNU Health HMIS: Libre Hospital Management and Health Information System
========================================================================

GNU Health party FIUNER module
------------------------------
Este modulo:

* modifica el campo de selección Education agragando nuevos elementos de acuerdo a los grados de educación de Argentina, en las tablas correspondientes a terceros.

* agrega un campo función edad para poder ver en un formato distinto la misma, además de definir la correspondiente función de búsqueda de la misma.

* agrega la fecha de nacimiento a la vista de arbol de paciente.

* trae el dato del barrio y su correspondiente función de búsqueda a la vista de paciente.

* agrega un campo tipo Char para identificar el número de historia clínica del paciente.

* agrega un campo tipo selección para discriminar pacientes crónicos de aquellos que no lo son.

* agrega un campo que permite setear y actualizar el número de telefono desde la vista de paciente.

* agrega un icono y una función para determinar paciente georreferenciados de aquellos que no lo son.

This module add support to:
* modify the Education selection field by adding new elements according to the degrees of education in Argentina, in the tables corresponding to third parties.

* adds "edad" function field to be able to see it in a different format, in addition to defining the corresponding search function for it.

* add date of birth to patient tree view.

* brings the neighborhood data and its corresponding search function to the patient view.

* adds a field type Char to identify the patient's medical record number.

* adds a selection type field to discriminate chronic patients from those who are not.

* adds a field that allows setting and updating the phone number from the patient view.

* adds an icon and a function to determine georeferenced patients from those who are not.


Documentation
-------------
* GNU Health

Wikibooks: https://en.wikibooks.org/wiki/GNU_Health/

Contact
-------
* GNU Health Contact 

 - website: https://www.gnuhealth.org
 - email: info@gnuhealth.org
 - Twitter: @gnuhealth

* FIUNER Contact 

 - email: saludpublica@ingenieria.uner.edu.ar

License
--------

GNU Health is licensed under GPL v3+::

 Copyright (C) 2008-2021 Luis Falcon <falcon@gnuhealth.org>
 Copyright (C) 2011-2021 GNU Solidario <health@gnusolidario.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


Prerequisites
-------------

 * Python 3.4 or later (http://www.python.org/)
 * Tryton 5.0 (http://www.tryton.org/)


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
