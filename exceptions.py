#This file is part health_patient_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.exceptions import UserError


class NoRecordSelected(UserError):
    pass


class NoDU(UserError):
    pass
