#This file is part health_patient_fiuner module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.

from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Or, Eval, Not, Bool, Equal
from trytond.tools import reduce_ids
from trytond.modules.health.core import get_institution

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.education.selection.insert(cls.education.selection.index(('0','None'))+1,('kindergarden','Jardin'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('kindergarden','Jardin'))+1,
                                   ('special_school','Escuela Especial'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('special_school','Escuela Especial'))+1,
                                   ('attending_primary_school','Primaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('2','Primary School'))+1,
                                   ('attending_secondary_school','Secundaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('4','Secondary School'))+1,
                                   ('attending_tertiary_school','Terciario Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_tertiary_school','Terciario Cursando'))+1,
                                   ('incomplete_tertiary_school','Terciario Incompleto'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('incomplete_tertiary_school','Terciario Incompleto'))+1,
                                   ('tertiary_school','Terciario'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('tertiary_school','Terciario'))+1,
                                   ('attending_university','Universidad Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_university','Universidad Cursando'))+1,
                                   ('incomplete_university','Universidad Incompleta'))


class PatientData(metaclass = PoolMeta):
    'Datos Paciente'
    __name__ = 'gnuhealth.patient'

    age_float = fields.Function(
        fields.Float('Age'),'get_age_float',
        searcher='search_age_float')
    dob = fields.Function(fields.Date('DoB'), 'get_patient_dob')
    barrio = fields.Function(
        fields.Char('Barrio'), 'get_patient_operational_sector', searcher='search_barrio')
    hc = fields.Char('HC', help='Paper Clinical History identifier')
    cronico = fields.Selection([
        (None, ''),
        ('Si', 'Si'),
        ('No', 'No'),
        ],'Cronico',help = 'Paciente con medicacion de cronico'
        , sort=False)
    phone = fields.Function(
        fields.Char('Phone',
            states={'readonly': Eval('state')=='done',}),
        'get_party_phone', setter='set_party_phone')
    georef = fields.Function(
        fields.Selection([
            (None,''),
            ('no-georef','No georef'),
            ('georef','Georef'),
            ],'Geo Ref'),'get_georef',searcher="search_geo_ref")
    reference_health_center = fields.Many2One(
        'gnuhealth.institution','Reference Health Center')

    def get_patient_dob(self, name):
        return self.name.dob

    def get_patient_operational_sector(self, name):
        if self.name.du:
            if self.name.du.operational_sector:
                return  self.name.du.operational_sector.name
        else:
            return 'Sin Dato'

    @classmethod
    def search_barrio(cls, name, clause):
        res = []
        value = clause[2]
        res.append(('name.du.operational_sector.name', clause[1], value))
        return res

    def get_age_float(self, name):
        dob = self.name.dob
        if self.name.dob:
            today = date.today()
            relative = relativedelta(today, dob)
            age_float = relative.years + relative.months/12 + relative.days/365
            age_float = round(age_float,3)
            return age_float
        return 0

    @classmethod
    def search_age_float(cls, name, clause):
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()
        _, operator,operand1  = clause

        curr_year = date.today().year

        pool = Pool()
        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')

        patient = Patient.__table__()
        party = Party.__table__()
        result1 = []
        result2 = []

        if operator in {'ilike','='}:
            query1 = (party.select(party.id,
                        where=((party.dob==None)
                                &(party.is_patient==True))))
            if operand1 != 0:
                operand2_first_day = date.today()-timedelta((float(operand1))*365)
                operand2_last_day = date.today()-timedelta(float(operand1+1)*365)
                query1 = (party.select(party.id,
                        where=((party.dob <=operand2_first_day)
                                &(party.dob >=operand2_last_day)
                                &(party.is_patient==True))))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
        else:
            #operand2 = date.today().replace(year=curr_year-operand1-1)
            operand2 = date.today()-timedelta(float(operand1)*365)
            Operator = fields.SQL_OPERATORS[clause[1]]
            query1 = (party.select(party.id,
                    where=((Operator(operand2,party.dob))
                            &(party.is_patient==True))))
            cursor.execute(*query1)
            result1 = cursor.fetchall()

        if result1:
            query2 = (patient.select(patient.id,
                where=(patient.name.in_(result1))))
            cursor.execute(*query2)
            result2 = cursor.fetchall()
            return [('id','in',[x[0] for x in result2])]
        return [('id','=',0)]

    @classmethod
    def get_party_phone(cls, patient, name=None):
        result = {}
        for a in patient:
            result[a.id] = a.name.phone or ''
        return result

    @classmethod
    def set_party_phone(cls, patient, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')
        for a in patient:
            if not a.name:
                continue
            party_phone = ContactMechanism.search([
                ('party', '=', a.name),
                ('type', '=', 'phone'),
                ])
            if party_phone:
                ContactMechanism.write(party_phone, {
                    'value': value,
                    })
            else:
                ContactMechanism.create([{
                    'party': a.name.id,
                    'type': 'phone',
                    'value': value,
                    }])

    def get_georef(self,name):
        if self.name.du and self.name.du.latitude and self.name.du.longitude:
            return 'georef'
        return None

    @classmethod
    def search_geo_ref(cls, name, clause):
        pool = Pool()
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()

        Party = pool.get('party.party')
        Patient = pool.get('gnuhealth.patient')
        DU = pool.get('gnuhealth.du')

        party = Party.__table__()
        patient = Patient.__table__()
        du = DU.__table__()

        result0 = []
        result1 = []
        if clause[2] == 'no-georef' or clause[2] == 'georef':
            #fetch all georef dus
            query0 = (du.select(du.id,
                        where=(du.latitude!=None)
                            &(du.longitude!=None)
                            ))
            cursor.execute(*query0)
            result0 = cursor.fetchall()
            #fetch all georef parties
            query1 = (party.select(party.id,
                        where=(reduce_ids(party.du,[x[0] for x in result0]))
                        ))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
            #fetch all georef patients
            query2 = (patient.select(patient.id,
                            where=(reduce_ids(patient.name,[x[0] for x in result1]))
                                  ))
            cursor.execute(*query2)
            result2 = cursor.fetchall()
            if clause[2]== 'georef':
                pass
            elif clause[2]=='no-georef':
                #fetchall no georef patients
                query3 = (patient.select(patient.id))
                cursor.execute(*query3)
                result3 = cursor.fetchall()
                result2 = list(set(result2)^set(result3))
        return [('id','in',[x[0] for x in result2])]

    @staticmethod
    def default_reference_health_center():
        pool = Pool()
        HealthInstitution = pool.get('gnuhealth.institution')
        return get_institution()

    @classmethod
    def __setup__(cls):
        super(PatientData,cls).__setup__()
        cls.education.selection.insert(cls.education.selection.index(('0','None'))+1,('kindergarden','Jardin'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('kindergarden','Jardin'))+1,
                                   ('special_school','Escuela Especial'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('special_school','Escuela Especial'))+1,
                                   ('attending_primary_school','Primaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('2','Primary School'))+1,
                                   ('attending_secondary_school','Secundaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('4','Secondary School'))+1,
                                   ('attending_tertiary_school','Terciario Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_tertiary_school','Terciario Cursando'))+1,
                                   ('incomplete_tertiary_school','Terciario Incompleto'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('incomplete_tertiary_school','Terciario Incompleto'))+1,
                                   ('tertiary_school','Terciario'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('tertiary_school','Terciario'))+1,
                                   ('attending_university','Universidad Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_university','Universidad Cursando'))+1,
                                   ('incomplete_university','Universidad Incompleta'))


class PatientSESAssessment(metaclass = PoolMeta):
    'Socioeconomics and Family Functionality Assessment'
    __name__ = 'gnuhealth.ses.assessment'

    @classmethod
    def __setup__(cls):
        super(PatientSESAssessment, cls).__setup__()
        cls.education.selection.insert(cls.education.selection.index(('0','None'))+1,('kindergarden','Jardin'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('kindergarden','Jardin'))+1,
                                   ('special_school','Escuela Especial'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('special_school','Escuela Especial'))+1,
                                   ('attending_primary_school','Primaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('2','Primary School'))+1,
                                   ('attending_secondary_school','Secundaria Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('4','Secondary School'))+1,
                                   ('attending_tertiary_school','Terciario Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_tertiary_school','Terciario Cursando'))+1,
                                   ('incomplete_tertiary_school','Terciario Incompleto'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('incomplete_tertiary_school','Terciario Incompleto'))+1,
                                   ('tertiary_school','Terciario'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('tertiary_school','Terciario'))+1,
                                   ('attending_university','Universidad Cursando'))
        cls.education.selection.insert(cls.education.selection.index(
                                   ('attending_university','Universidad Cursando'))+1,
                                   ('incomplete_university','Universidad Incompleta'))

    @classmethod
    @ModelView.button
    def end_assessment(cls, assessments):
        super(PatientSESAssessment,cls).end_assessment(assessments)
        pool = Pool()
        Party = pool.get('party.party')
        for assessment in assessments:
            party = Party.search([('id','=',assessment.patient.name.id)])
            party[0].education = assessment.education
            party[0].occupation = assessment.occupation
            party[0].save()


class Appointment(metaclass=PoolMeta):
    'Patient Appointments'
    __name__ = 'gnuhealth.appointment'

    reference_health_center = fields.Function(
        fields.Many2One('gnuhealth.institution','Reference Health Center'),
            'get_reference_health_center')
            #'get_reference_health_center', setter='set_reference_health_center')

    @classmethod
    def get_reference_health_center(cls, appointment, name=None):
        result = {}
        for app in appointment:
            if app.patient:
                result[app.id] = app.patient.reference_health_center and app.patient.reference_health_center.id or None
            else:
                result[app.id] = None
        return result

    #def set_reference_health_center(self, appointment, name, reference_health_center):
        #pool = Pool()
        #Patient = pool.get('gnuhealth.patient')
        #for app in appointment:
            #if not app.patient:
                #continue
            #Patient.write([app.patient.id],{
                #'reference_health_center': reference_health_center,
                #})

